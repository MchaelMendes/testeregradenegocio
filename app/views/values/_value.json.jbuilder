json.extract! value, :id, :value1, :value2, :created_at, :updated_at
json.url value_url(value, format: :json)
