class CreateValues < ActiveRecord::Migration
  def change
    create_table :values do |t|
      t.float :value1
      t.float :value2

      t.timestamps null: false
    end
  end
end
